<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Erinnerung_Ende_Probezeit_an_Management</fullName>
        <description>Erinnerung Ende Probezeit an Management</description>
        <protected>false</protected>
        <recipients>
            <recipient>bs@weitclick.de</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>filiz.tincman@weitclick.de</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>frank.boegner@weitclick.de</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mb@weitclick.de</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>pth@weitclick.de</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>su@weitclick.de</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Ende_Probezeit</template>
    </alerts>
    <rules>
        <fullName>Erinnerung Probezeit</fullName>
        <active>true</active>
        <description>eMail Erinnerung Probezeit an Management =&gt; 8 Wochen vor Ablauf</description>
        <formula>LEN(Text (Ende_Probezeit__c))&gt;0</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Erinnerung_Ende_Probezeit_an_Management</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Personal__c.Ende_Probezeit__c</offsetFromField>
            <timeLength>-56</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
