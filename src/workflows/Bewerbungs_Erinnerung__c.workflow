<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Bewerbungs_Erinnerg</fullName>
        <description>Bewerbungs-Erinnerung</description>
        <protected>false</protected>
        <recipients>
            <field>Erinnerung_an__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Bewerbung_Erinnerung</template>
    </alerts>
    <rules>
        <fullName>Bewerbungs_Erinnerung</fullName>
        <active>true</active>
        <booleanFilter>1</booleanFilter>
        <criteriaItems>
            <field>Bewerbungs_Erinnerung__c.Erinnerung_am__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>eMail Erinnerung, die versendet wird, wenn auf einer Bewerbung ein Objekt &quot;Bewerbungs-Erinnerung&quot; angelegt wurde.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Bewerbungs_Erinnerg</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Bewerbungs_Erinnerung__c.Erinnerung_am__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
