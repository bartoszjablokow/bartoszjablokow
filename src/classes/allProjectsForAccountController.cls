public class allProjectsForAccountController {
    
    public List<Projekt__c> listOfAssignedProjects {
        get {
                return  [SELECT Id, Name, OwnerId, Status__c, RecordTypeId, Record_Type_Icon__c FROM Projekt__c WHERE Account__c = :currentAccountId];      
            } 
            set;}
    
    
    public List<Projekt__c> listOfAssignedProjectsActiveOnly {
        get {
            return  [SELECT Id, Name, OwnerId, Status__c, RecordTypeId, Record_Type_Icon__c FROM Projekt__c WHERE Account__c = :currentAccountId AND Status__c != 'Abgeschlossen'];     
        } 
        set;}

    public Account currentAccount {get; set;}

    public Id currentAccountId {get; set;}

    public String sfBaseUrl {get; set;}

    public String apexPageRef {get; set;}

    public String apexFullListRef {get; set;}

    public Boolean filterCheckbox {get; set;}

    public Integer noOfProjectRecords {
        get {
            return listOfAssignedProjects.size();
        }
        set;}

    public List<Projekt__c> finalListOfAssignedProjects { get; set; }

    public allProjectsForAccountController(ApexPages.StandardController std) {
        this.currentAccountId = std.getId();
        //queryListOfRelatedProjects(currentAccountId);
        getBaseUrl();
        this.finalListOfAssignedProjects = listOfAssignedProjects;
    }

    public void setFinalListOfProjects() {
        System.debug('filterCheckbox = '+filterCheckbox);
        if(filterCheckbox == false) {
            this.finalListOfAssignedProjects = listOfAssignedProjects;
            } else {
            this.finalListOfAssignedProjects = listOfAssignedProjectsActiveOnly;
            }
    }
/*
    private void queryListOfRelatedProjects(Id idOfCurrentAccount) {
        this.listOfAssignedProjects = [SELECT Id, Name, OwnerId, Status__c, RecordTypeId, Record_Type_Icon__c FROM Projekt__c WHERE Account__c = :idOfCurrentAccount];      
    }

    private void queryListOfRelatedProjectsActiveOnly(Id idOfCurrentAccount) {
        this.listOfAssignedProjects = [SELECT Id, Name, OwnerId, Status__c, RecordTypeId, Record_Type_Icon__c FROM Projekt__c WHERE Account__c = :idOfCurrentAccount AND Status__c != 'Abgeschlossen'];     
    }
*/
    private void getBaseUrl() {
        this.sfBaseUrl = URL.getSalesforceBaseUrl().toExternalForm();
        this.apexPageRef = '/apex/projectTreeViewPage?scontrolCaching=1&id=';
        this.apexFullListRef = '/apex/allProjectsForAccountPage?scontrolCaching=1&id=';
    }
}