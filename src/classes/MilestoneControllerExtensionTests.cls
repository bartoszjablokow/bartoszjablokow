@isTest 
private class MilestoneControllerExtensionTests {


   public static testMethod void testMyController() {
       Projekt__c pc = new Projekt__c();
       
       
       
       
       ApexPages.StandardController sc = new ApexPages.StandardController(pc);
       MilestoneControllerExtension me = new MilestoneControllerExtension(sc);
       
       PageReference pr = new PageReference('/apex/MilestoneBearbeitung?id=1');
       me.termineMilestonesSorted = Database.query('SELECT Name, Id, Datum__c, Erledigt__c, Verantwortlich__c, Termin__c, Kommentar__c, To_Do_f_r_Termin__c, Nicht_im_Report__c FROM Termine_Milestones__c order by Datum__c desc limit 2');
       pr = me.save();
       me.projectId = 'a0Ig0000000LYw8';
       me.termineMilestonesSorted = me.getTermineMilestonesSorted();
       System.assertEquals(1, 1);
   }
   
   
   
}