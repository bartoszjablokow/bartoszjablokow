public without sharing class projectTreeViewController {

    private final Projekt__c prc; // current Projekt__c
    
    public transient String JSONString {get; private set;} // first JSON String to build the tree
    
    public transient list<Projekt__c> prcs {get; private set;} // list of retrieved projects
    
    public ID ProjectId {get; set;} // current Projekt__c ID

    //public ID RootAccountId {get; set;} // root Projekt__c ID

    public transient ProjectNode rootNode {get; set;} // root Projekt__c Node

    public list<list<Projekt__c>> prcs_list {get; set;}

    public list<map<ID, ProjectNode>> listMapAN {get; set;} // list of levels with maps for nodes

    public String projectSelectionWay { get; set; }

    //Chunk
    public String prcParentId_param {get; set;} // input parameter for getChunk method

    public transient String JSONString_param {get; set;} // output parameter fo gethunk method

    public Integer chunk_limit {get; set;}  // the limit of Projekt__c record during pulling data

    public List<Projekt__c> prcs_param {get; set;}

    private Integer max_levels = 20; // limit of levels

    private Integer max_sibling = 100; // limit for one level
    
    public Boolean isChunkredy{get;set;}
    
    public String workProgress{get; set;}

    public String selectedProjects {get; set;}
    
    public String delId {get; set;}
    
    public String searchName { get; set; }

    //public String searchERP { get; set; }
    
    public String epSearchName { get; set; }

    //public String epSearchERP { get; set; }

    public Contact contact { get; private set; }
    
    public List<Projekt__c> projects { get; private set; }

    public Projekt__c currentProject;

    public String sfBaseUrl {get; set;}
    
    public projectTreeViewController(ApexPages.StandardController std) { // constructor
            // get Id from URL.
            //String id = ApexPages.currentPage().getParameters().get('ContactId');
            //String prid = ApexPages.currentPage().getParameters().get('ProjectId');
            
            this.currentProject = (Projekt__c)std.getRecord();
            String prid = currentProject.Id;
            setSfBaseUrl();
            System.debug(Logginglevel.ERROR,'Contact => '+ prid);

            
            this.ProjectId = prid;
            this.prc = getProjectData(this.ProjectId);
            
            AccHierarchyController(); // additonal settings 
            this.projectSelectionWay = 'Project hierarchy';
        }

    public void AccHierarchyController() {
        this.prcs = new list<Projekt__c>();
        this.prcs_param = new list<Projekt__c>();
        this.rootNode = new ProjectNode(this.prc);
        this.prcs_list = new list<list<Projekt__c>>();
        this.rootNode = this.rootNode.buildRootForNode(); // add nodes from current to the root
        this.listMapAN = getDataForLevels();
        //this.buildAdditionalNodes(); // add additional limited nodes for limited levels
        
        this.prcs.addAll(this.rootNode.getProjectList());
        system.debug('this.prcsFinal = ' + this.prcs);
        //list<Projekt__c> accsTemp = new list<Projekt__c>();

        this.JSONString = JSON.serialize(this.rootNode);
        
        this.JSONString_param = JSON.serialize(this.rootNode);
        this.chunk_limit = 20;

        System.debug('BJDEBUG this.prc'+this.prc);
        System.debug('BJDEBUG rootNode object'+rootNode);
        System.debug('BJDEBUG rJSONString'+ JSONString);
        System.debug('BJDEBUG_JSONString_param after: '+this.JSONString_param);

    } 
    
     // get Acouunt data for its ID
    public Projekt__c getProjectData(ID id) {
        Projekt__c prc = [Select  Id, Name, Project_Parent__c, OwnerId, Status__c
                        From    Projekt__c
                        Where   Id = :Id ];  
        return prc; 
    }

    // method for set base url
    public void setSfBaseUrl() {
        sfBaseUrl = URL.getSalesforceBaseUrl().toExternalForm();     
    }
    
    // action for pull new data for tree (4 AJAX)
    
    public PageReference getChunk2() {
        try {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Chunk Started'));
            
            workProgress = 'Started';
            system.debug('JSONString_param before: ' + this.JSONString_param);
            system.debug('prcParentId_param: ' + prcParentId_param);
            system.debug('this.prcs_param: ' + this.prcs_param);
            system.debug('this.prcs: ' + this.prcs);
            System.debug('is listMapAN : ' + listMapAN);
            ProjectNode an_temp;
            ProjectNode an_temp2;
            list<ProjectNode> listTemp = new list<ProjectNode>();
            an_temp = new ProjectNode();
            for (map<ID, ProjectNode> mapAN : listMapAN) {
                system.debug('mapAN: ' + mapAN);
                system.debug('an_temp: ' + an_temp);
                an_temp = mapAN.get(prcParentId_param);
                system.debug('mapAN: ' + mapAN);
                system.debug('an_temp: ' + an_temp);
                if (an_temp != null) break;
            }
            system.debug('an_temp: ' + an_temp);
            Integer level = an_temp.level + 1;
            //list<ID> parentsIds = listMapAN[level].keySet();\
            Set<ID> retrievedIds;
            map<ID, ProjectNode> mapTemp;
            if (listMapAN.size() > level) {
                mapTemp = listMapAN[level];
                retrievedIds = mapTemp.keySet();
            }
            else
                mapTemp = new map<ID, ProjectNode>();
            
            this.prcs_param = [ Select  Id, Name, Project_Parent__c, OwnerId, Status__c
                        From    Projekt__c
                                Where   Project_Parent__c = :an_temp.prc.Id//this.prcParentId_param
                                    and Id not in :retrievedIds
                                order by    Name, CreatedDate
                                limit   :this.chunk_limit];
            list<AggregateResult> children = [ Select  Project_Parent__c
                                        From    Projekt__c
                                        Where   Project_Parent__c = :getIds(this.prcs_param)
                                            //and Id not in :retrievedIds
                                        group by    Project_Parent__c];
                                        //limit   :this.chunk_limit];
            Set<ID> idsWithNoChildren = new Set<ID>();
            //idsWithNoChildren = new Set<ID>(clsSobjectList.getIds(children));
            idsWithNoChildren.addAll(getIds(children));
            
            for (Projekt__c prc : prcs_param) {
                an_temp2 = new ProjectNode(prc);
                an_temp2.level = level;
                if (!idsWithNoChildren.contains(an_temp2.prc.Id))
                    an_temp2.noChildren = true;
                system.debug('### an_temp2: ' + an_temp2);
                system.debug('### listTemp: ' + listTemp);
                listTemp.add(an_temp2);
                mapTemp.put(an_temp2.prc.Id, an_temp2);
                listMapAN.get(level-1).get(an_temp2.prc.Id);
            }

            mapTemp = new map<ID, ProjectNode>();
            mapTemp.put(an_temp.prc.ID, an_temp);   

            //this.prcs.addAll(this.prcs_param);
            //this.JSONString_param = JSON.serialize(this.prcs_param);
            this.JSONString_param = JSON.serialize(listTemp);
            system.debug('BJDEBUG_JSONString_param after: ' + this.JSONString_param);
        }
        catch (System.QueryException e) {
            this.JSONString_param = null;
        }
        return null;
    } 
    
    private List<ID> getIds(List<SObject> SObjects) {
        List<ID> Ids = new List<ID>();
        for (SObject so : SObjects) {
            Ids.add(so.Id);
        }
        return Ids;
    }

    private list<map<ID, ProjectNode>> getDataForLevels() {
        list<map<ID, ProjectNode>> listMapAN = new list<map<ID, ProjectNode>>();
        ProjectNode an_temp = this.rootNode;
        map<ID, ProjectNode> mapTemp = new map<ID, ProjectNode>();
        mapTemp.put(an_temp.prc.ID, an_temp);
        listMapAN.add(mapTemp);
        list<Projekt__c> prcs;
        Set<ID> parentsIds;
        ProjectNode an_temp2;
        Integer level = 0;
        Integer max_sibling2;
        // for limited levels
        //an_temp = new AccountNode();
        ID temp_ID;
        for (level = 0; level < this.max_levels; level++) {
            system.debug('### level: ' + level);
            /*system.debug('### an_temp.prc.ID: ' + an_temp.prc.ID);
            system.debug('### an_temp.prc.Name: ' + an_temp.prc.Name);
            system.debug('### an_temp: ' + an_temp);*/
            
            mapTemp = new map<ID, ProjectNode>();
            System.debug('#BJDEBUG an_temp 1 = '+an_temp);
            if (an_temp != null) {
                if (an_temp.prcs.size() > 0) { // if already has children
                    system.debug('### if (an_temp.prcs.size() > 0)');
                    an_temp = an_temp.prcs.get(0);
                    System.debug('#BJDEBUG an_temp 2 = '+an_temp);
                    an_temp.level = level+1;
                    mapTemp.put(an_temp.prc.ID, an_temp);
                    max_sibling2 = max_sibling - 1;
                } else {
                    an_temp = null;
                    max_sibling2 = max_sibling;
                }
                an_temp2 = an_temp;
            }
                
            system.debug('### an_temp: ' + an_temp);

            //if (level < listMapAN.size())
            parentsIds = listMapAN[level].keySet();

            system.debug('### parentsIds: ' + parentsIds);
            //system.debug('### an_temp.prc.ID: ' + an_temp.prc.ID);
           // system.debug('### an_temp.prc.Name: ' + an_temp.prc.Name);
            system.debug('### max_sibling+1: ' + max_sibling+1);
            if (an_temp != null) {
                temp_ID = an_temp.prc.ID;
            } else {
                temp_ID = null;
            }
                
            system.debug('### temp_ID: ' + temp_ID);
            prcs = [ Select  Id, Name, Project_Parent__c, OwnerId, Status__c
                        From    Projekt__c
                    Where   Project_Parent__c in :parentsIds and Id <> :temp_ID
                    Limit   :max_sibling2+1 ];
            system.debug('### prcs: ' + prcs);
            
            if (prcs.size() == max_sibling2+1) {
                prcs.remove(max_sibling2);
            } else {
                //todo: setNoChildren(listMapAN[level]); // set noChildren to True for parents nodes
                for (ProjectNode an : listMapAN[level].Values()) {
                    an.noChildren = true;
                }
            }
            
            for (Projekt__c prc1 : prcs) {
                
                an_temp2 = new ProjectNode(prc1);
                an_temp2.level = level+1;
                listMapAN.get(level).get(prc1.Project_Parent__c).prcs.add(an_temp2); // add child to parent node level
                mapTemp.put(prc1.Id, an_temp2); // add node to current node level
            }
            
            listMapAN.add(mapTemp);
        }

        system.debug('### an_temp: ' + an_temp);
        system.debug('### an_temp2: ' + an_temp2);
        an_temp = an_temp2;
        if (an_temp != null)
            while (an_temp.prcs.size() > 0) {
                an_temp2 = an_temp.prcs.get(0);
                an_temp2.level = ++level;
                //listMapAN.get(level-1).get(an_temp.prc.ID).prcs.add(an_temp2); // add child to parent node level
                mapTemp.put(an_temp2.prc.Id, an_temp2); // add node to current node level
                listMapAN.add(mapTemp);
                an_temp = an_temp2;
            }
        
        return listMapAN;
    }

 // ------------------------------------------------------- WRAPPER CLASSES DEFINITIONS -------------------------------------------------------
   /************** project Wrapper *************/
    public class ProjectNode {
        public Projekt__c prc;
        public Id ProjectId;
        public list<ProjectNode> prcs;
        public Boolean noChildren;
        public Integer level;
        

        //default constructor
        public ProjectNode() {
            this.prcs = new list<ProjectNode>();
        }
        //Constructor for Projects
        public ProjectNode(Projekt__c prc) {
            this.prc = prepareNode(prc);
            this.ProjectId = prc.Id;
            this.prcs = new list<ProjectNode>();

        }
        /*
        public Projekt__c prepareNode(Projekt__c rootNode) {
            //return rootNode;
            string tempValue = '';
            
            list<String> specialCharacters = new List <String>{'\"', '\'', '\\}', '\\{'}; 
            integer index = 0;
            string notSpecial='';
            
            //list<String> prcQueriedFields = new list<String>{'Name','BillingStreet', 'BillingPostalCode'};
            list<String> prcQueriedFields = new list<String>{'Name', 'OwnerId', 'Status__c'};
            for (String entryKey : prcQueriedFields) {
                //system.debug('xxx_checkme ' + rootNode.get(entryKey));
                if(rootNode.get(entryKey)!= null){
                    
                    tempValue = (string)rootNode.get(entryKey);
                    system.debug('xxx_Before - tempValue = ' + tempValue);
                    
                    for (String special : specialCHaracters){
                            notSpecial = '\\' + special;
                        tempValue = tempValue.replace(special, notSpecial);
                        //system.debug('xxx_tempValue = ' + tempValue);
                        notSpecial='';
                    }
                    
                    system.debug('xxx_After - tempValue = ' + tempValue);
                    
                    rootNode.put(entryKey, tempValue);
                    tempValue='';
                }
            }
            //system.debug('xxx_rootNode.prc = ' + rootNode);
            return rootNode;
        }

        
        public Projekt__c prepareNode(Projekt__c rootNode) {
            //return rootNode;
            string tempValue = '';
            
            list<String> specialCharacters = new List <String>{'\"', '\'', '\\}', '\\{'}; 
            integer index = 0;
            string notSpecial='';
            
            //list<String> prcQueriedFields = new list<String>{'Name','BillingStreet', 'BillingPostalCode'};
            list<String> prcQueriedFields = new list<String>{'Name', 'OwnerId', 'Status__c'};
            for (String entryKey : prcQueriedFields) {
                //system.debug('xxx_checkme ' + rootNode.get(entryKey));
                if (!(entryKey.containsAny('.'))) {
                    if(rootNode.get(entryKey) != null){
                        system.debug('entryKey ' + entryKey + ' doesnt contains any dots');
                        tempValue = (string)rootNode.get(entryKey);
                        system.debug('xxx_Before - tempValue = ' + tempValue);
                        
                        for (String special : specialCHaracters){
                                notSpecial = '\\' + special;
                            tempValue = tempValue.replace(special, notSpecial);
                            //system.debug('xxx_tempValue = ' + tempValue);
                            notSpecial='';
                        }
                        
                        system.debug('xxx_After - tempValue = ' + tempValue);
                        
                        rootNode.put(entryKey, tempValue);
                        System.debug('rootnode = '+rootNode);
                        tempValue='';
                    } 
                } else {
                    List<String> fieldParts = entryKey.splitByCharacterTypeCamelCase();

                    if(rootNode.getSObject(fieldParts[0]).get(fieldParts[2]) != null){
                        system.debug('entryKey ' + entryKey + ' contains any dots');
                        tempValue = (string)rootNode.getSObject(fieldParts[0]).get(fieldParts[2]);
                        system.debug('xxx_Before - tempValue = ' + tempValue);
                        
                        for (String special : specialCHaracters){
                                notSpecial = '\\' + special;
                            tempValue = tempValue.replace(special, notSpecial);
                            //system.debug('xxx_tempValue = ' + tempValue);
                            notSpecial='';
                        }
                        
                        system.debug('xxx_After - tempValue = ' + tempValue);
                        
                        rootNode.put(entryKey, tempValue);
                        System.debug('rootnode = '+rootNode);
                        tempValue='';
                    }    
                }    
            }
            //system.debug('xxx_rootNode.prc = ' + rootNode);
            return rootNode;
        } */
        public Projekt__c prepareNode(Projekt__c rootNode) {
            //return rootNode;
            string tempValue = '';
            
            list<String> specialCharacters = new List <String>{'\"', '\'', '\\}', '\\{'}; 
            integer index = 0;
            string notSpecial='';
            String ownerName = getUserFullName(rootNode);
            //list<String> prcQueriedFields = new list<String>{'Name','BillingStreet', 'BillingPostalCode'};
            list<String> prcQueriedFields = new list<String>{'Name', 'OwnerId', 'Status__c'};
            for (String entryKey : prcQueriedFields) {
                //system.debug('xxx_checkme ' + rootNode.get(entryKey));
                if (!(entryKey.contains('OwnerId'))) {
                    if(rootNode.get(entryKey) != null){
                        system.debug('entryKey ' + entryKey + ' doesnt contains any dots');
                        tempValue = (string)rootNode.get(entryKey);
                        system.debug('xxx_Before - tempValue = ' + tempValue);
                        
                        for (String special : specialCHaracters){
                                notSpecial = '\\' + special;
                            tempValue = tempValue.replace(special, notSpecial);
                            //system.debug('xxx_tempValue = ' + tempValue);
                            notSpecial='';
                        }
                        
                        system.debug('xxx_After - tempValue = ' + tempValue);
                        
                        rootNode.put(entryKey, tempValue);
                        System.debug('rootnode = '+rootNode);
                        tempValue='';
                    } 
                } else {
                    
                    if(rootNode.get(entryKey) != null){
                        system.debug('entryKey ' + entryKey + ' contains any dots');
                        tempValue = ownerName;
                        system.debug('xxx_Before - tempValue = ' + tempValue);
                        
                        for (String special : specialCHaracters){
                                notSpecial = '\\' + special;
                            tempValue = tempValue.replace(special, notSpecial);
                            //system.debug('xxx_tempValue = ' + tempValue);
                            notSpecial='';
                        }
                        
                        system.debug('xxx_After - tempValue = ' + tempValue);
                        
                        rootNode.put(entryKey, tempValue);
                        System.debug('rootnode = '+rootNode);
                        tempValue='';
                    }    
                }    
            }
            //system.debug('xxx_rootNode.prc = ' + rootNode);
            return rootNode;
        }
        
        //pull User names from users
        public String getUserFullName (Projekt__c prc) {
            
            String pr = [ Select  Id, OwnerId, Owner.Name
                            From    Projekt__c
                            Where   Id = :prc.Id].Owner.Name;
            System.debug('pr = '+pr);
            return pr;
        }

        // pull data from parent from DB
        public ProjectNode pullParent(ProjectNode prcn) {
            Projekt__c prc = [ Select  Id, Name, Project_Parent__c
                            From    Projekt__c
                            Where   Id = :prcn.prc.Project_Parent__c];
            ProjectNode prcn_parent = new ProjectNode(prc); 
            return prcn_parent;
        }

        // pull/get data for parent Node from parent Projekt__c
        public ProjectNode pullParent() {
            Projekt__c prc;
            try {
                prc = [ Select  Id, Name, Project_Parent__c, OwnerId, Status__c
                        From    Projekt__c
                        Where   Id = :this.prc.Project_Parent__c ];
            }
            catch (System.QueryException e) {
                return null;
            }
            return new ProjectNode(prc);
        }

        // build parent for node
        public ProjectNode buildParent() {
            ProjectNode prcn_parent = this.pullParent();
            
            if (prcn_parent != null) {
                prcn_parent.prcs.add(this);
                //this = accn_parent.clone();
            }
            return prcn_parent;
        }

        
        // build minimal node hierarchy from current node to the root
        public ProjectNode buildRootForNode() {
            ProjectNode prcn_root = this;
            //accn_root.addVirtualChild();
            for (ProjectNode prcn_temp = this; prcn_temp != null; prcn_temp = prcn_temp.buildParent()) {
                prcn_root = prcn_temp;
            }
            prcn_root.level = 0;
            return prcn_root;
        }
 
        
        // get Acounts from current node and from children (recursive)
        public list<Projekt__c> getProjectList() {
            list<Projekt__c> prcs = new list<Projekt__c>();
            for (ProjectNode pcn : this.prcs) {
                prcs.addAll(pcn.getProjectList());
            }
            prcs.add(prc);
            return prcs;
        }

        // add child node - unused
        public ProjectNode addChildNode(ProjectNode childNode) {
            prcs.add(childNode);
            return this;
        }

    }
/************** customized project Wrapper end *************/
  
}