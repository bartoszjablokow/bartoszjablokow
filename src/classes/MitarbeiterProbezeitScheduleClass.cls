global class MitarbeiterProbezeitScheduleClass implements 
   Schedulable {

   global String query;
   
   
    global void execute(SchedulableContext ctx){

        
    query = 'SELECT Nachname__c, Vorname__c, Id,CreatedDate,Vertragsstart__c,letztesGespraechAlert__c FROM Personal__c where Ende_Probezeit__c = NEXT_N_DAYS:30 AND Typ__c = \'Mitarbeiter\' AND (letztesGespraechAlert__c < LAST_N_DAYS:14 OR letztesGespraechAlert__c = NULL) LIMIT 8';
    List<SObject> scope = Database.query(query);
    
        for(sObject s: scope) 
        {
            Personal__c p = (Personal__c) s;
            // Zeit seit letztem MEG berechnen
            // letztes MEG holen
            checkNotify(p);
            
      } 
        
       
   }
   
      
   global void checkNotify(Personal__c p){
         String megQuery;
         megQuery = 'select Id from Mitarbeitergespr_ch__c where Gespr_ch_am__c = LAST_N_DAYS:365 AND Personal__c = \'' + p.Id + '\'';
         List<Mitarbeitergespr_ch__c> megs = Database.query(megQuery);
         System.debug('anzahl gespr' + megs.size() + 'EOM');
 
         if(megs.size() == 0){
            notify(p.Vorname__c + ' ' + p.Nachname__c, 'ok', p.Id);
            p.letztesGespraechAlert__c = Date.today();
            update p;
         }
   }
       
       

       
   global void notify(String maName, String letztesGespraech, String pId){
       
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       mail.setToAddresses(new List<String>{'fb@weitclick.de', 'job@weitclick.de'});
       mail.setSubject('Mitarbeitergespräch (Probezeit) ist fällig für: ' + maName);
       mail.setPlainTextBody
       ('Gespräch für Mitarbeiter innerhalb der Probezeit: ' + maName + '\nhttps://eu3.salesforce.com/' + pId + '\n\n\nHinweis: Letztes Gespräch liegt entweder länger als 1 Jahr zurück oder es wurde noch nie ein Gespräch geführt. Es werden alle Mitarbeiter berücksichtigt, die länger als 10 Monate dabei sind. Diese Erinnerung wiederholt sich alle 14 Tage, solange bis ein Mitarbeitergespräch geführt wurde.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
   }
       
       
       global void myFunc1(){
           String t1 = '123';
           String t2 = '232';
           
       }
       
       global void myFunc2(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc3(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc4(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc5(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc6(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc7(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc8(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc1a(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc2a(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc3a(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc4a(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc5a(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc6a(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc7a(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc8a(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc1b(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc2b(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc3b(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc4b(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc5b(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc6b(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc7b(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc8b(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc1c(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc2c(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc3c(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc4c(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc5c(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc6c(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc7c(){
           String t1 = '123';
           String t2 = '232';
       }
       
       global void myFunc8c(){
           String t1 = '123';
           String t2 = '232';
       }

}