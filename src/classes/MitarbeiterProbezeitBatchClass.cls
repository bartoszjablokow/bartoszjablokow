// can be deleted
global class MitarbeiterProbezeitBatchClass implements 
   Database.Batchable<sObject> {

   global final String query;
   
    global MitarbeiterProbezeitBatchClass() {
        /*Alle bei denen der letzte Alert laenger als 14d her ist oder noch nie ein
        alert erfolgt ist */
        query = 'SELECT Nachname__c, Vorname__c, Id,CreatedDate,Vertragsstart__c,letztesGespraechAlert__c FROM Personal__c where Ende_Probezeit__c = NEXT_N_DAYS:30 AND Typ__c = \'Mitarbeiter\' AND (letztesGespraechAlert__c < LAST_N_DAYS:14 OR letztesGespraechAlert__c = NULL)';
    }

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }
   
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        final String megQuery;
        for(sObject s: scope) {
            Personal__c p = (Personal__c) s;
            // Zeit seit letztem MEG berechnen
            // letztes MEG holen
            megQuery = 'select Id from Mitarbeitergespr_ch__c where Gespr_ch_am__c = LAST_N_DAYS:365 AND Personal__c = \'' + p.Id + '\'';
            List<Mitarbeitergespr_ch__c> megs = Database.query(megQuery);
            System.debug('anzahl gespr' + megs.size() + 'EOM');
            if(megs.size() == 0){
                notify(p.Vorname__c + ' ' + p.Nachname__c, 'ok', p.Id);
                p.letztesGespraechAlert__c = Date.today();
                update p;
            }
            
            System.debug('time is' + DateTime.now().getTime() + 'EOM');

      }   
       
   }
   
   global void notify(String maName, String letztesGespraech, String pId){
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
       mail.setToAddresses(new List<String>{'fb@weitclick.de', 'frank.boegner@weitclick.de'});
       mail.setSubject('Mitarbeitergespräch (Probezeit) ist fällig für: ' + maName);
       mail.setPlainTextBody
       ('Gespräch für Mitarbeiter innerhalb der Probezeit: ' + maName + '\nhttps://eu3.salesforce.com/' + pId + '\n\n\nHinweis: Letztes Gespräch liegt entweder länger als 1 Jahr zurück oder es wurde noch nie ein Gespräch geführt. Es werden alle Mitarbeiter berücksichtigt, die länger als 10 Monate dabei sind. Diese Erinnerung wiederholt sich alle 14 Tage, solange bis ein Mitarbeitergespräch geführt wurde.');
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
   }

   global void finish(Database.BatchableContext BC){
       AsyncApexJob a = 
           [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            FROM AsyncApexJob WHERE Id =
            :BC.getJobId()];
                          
       
   }
}