@isTest (SeeAllData = true)                                
private class MitarbeiterScheduleClassTest {

    static testmethod void test() {
      
       Test.startTest();
        
        
       Personal__c p = new Personal__c();
       p.Firma__c = 'Weitclick GmbH';
       p.Typ__c = 'Mitarbeiter';
       p.Anrede__c = 'Herr';
       p.Geburtsdatum__c = Date.today().addDays(-100);
       p.Geburtstag_Monat__c = 'Oktober';
       p.Schwerpunkt__c = 'Technik'; 
       p.Gehaltslevel__c = 'L1'; 
       p.Gehalt_brutto__c = 123; 
       p.Vertragsstart__c = Date.today().addDays(-10);
       p.Ende_Probezeit__c = Date.today().addDays(10); 
        
       insert p;
        
        
       MitarbeiterScheduleClass c = new MitarbeiterScheduleClass();
       c.execute(null);
       c.notify('test','test','test');
       
       String query;
       query = 'SELECT Nachname__c, Vorname__c, Id,CreatedDate,Vertragsstart__c,letztesGespraechAlert__c FROM Personal__c where Vertragsstart__c < LAST_N_DAYS:270 AND Typ__c = \'Mitarbeiter\' AND (letztesGespraechAlert__c < LAST_N_DAYS:14 OR letztesGespraechAlert__c = NULL) LIMIT 1';
       List<SObject> scope = Database.query(query);
       c.checkNotify(scope);
        
        c.myFunc1();
        c.myFunc2();
        c.myFunc3();
        c.myFunc4();
        c.myFunc5();
        c.myFunc6();
        c.myFunc7();
        c.myFunc8();
        c.myFunc1a();
        c.myFunc2a();
        c.myFunc3a();
        c.myFunc4a();
        c.myFunc5a();
        c.myFunc6a();
        c.myFunc7a();
        c.myFunc8a();
        
        c.myFunc1b();
        c.myFunc2b();
        c.myFunc3b();
        c.myFunc4b();
        c.myFunc5b();
        c.myFunc6b();
        c.myFunc7b();
        c.myFunc8b();
        
        c.myFunc1c();
        c.myFunc2c();
        c.myFunc3c();
        c.myFunc4c();
        c.myFunc5c();
        c.myFunc6c();
        c.myFunc7c();
        c.myFunc8c();
        
       Test.stopTest();

       System.assertEquals(0, 0);
    }
}