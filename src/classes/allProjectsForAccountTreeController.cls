public without sharing class allProjectsForAccountTreeController {

    private final Projekt__c prc; // current Projekt__c
    
    public String JSONString {get; private set;} // first JSON String to build the tree

    public String JSONString2 {get; private set;} 
    
    public list<Projekt__c> prcs {get; private set;} // list of retrieved projects
    
    public ID ProjectId {get; set;} // current Projekt__c ID

    public Account rootNodeAcc;

    public  ProjectNode rootNode {get; set;} // root Projekt__c Node

    public list<list<Projekt__c>> prcs_list {get; set;}

    public list<map<ID, ProjectNode>> listMapAN {get; set;} // list of levels with maps for nodes

    public String projectSelectionWay { get; set; }

    //Chunk
    public String prcParentId_param {get; set;} // input parameter for getChunk method

    public String JSONString_param {get; set;} // output parameter fo gethunk method

    public Integer chunk_limit {get; set;}  // the limit of Projekt__c record during pulling data

    public List<Projekt__c> prcs_param {get; set;}

    private Integer max_levels = 20; // limit of levels

    private Integer max_sibling = 100; // limit for one level
    
    public Boolean isChunkredy{get;set;}
    
    public String workProgress{get; set;}

    public String selectedProjects {get; set;}
    
    public String delId {get; set;}
    
    public String searchName { get; set; }

    //public String searchERP { get; set; }
    
    public String epSearchName { get; set; }

    //public String epSearchERP { get; set; }

    public Contact contact { get; private set; }
    
    public List<Projekt__c> projects { get; private set; }

    public Projekt__c currentProject;

    public String sfBaseUrl {get; set;}

    public List<Projekt__c> rootsProjects;

    public List<String> fieldsForController = new List<String>{'Name','Id'};

    public Boolean filterCheckbox {get; set;}
    
    public allProjectsForAccountTreeController(ApexPages.StandardController std) { // constructor
            // get Id from URL.
            std.addFields(fieldsForController);
            this.rootNodeAcc = (Account)std.getRecord();
            //System.debug('filterCheckbox initial = '+filterCheckbox);
            setSfBaseUrl();
            PrcHierarchyController(); // additonal settings 
            this.projectSelectionWay = 'Project hierarchy';
    }

    public void PrcHierarchyController() {
        this.prcs = new list<Projekt__c>();
        this.prcs_param = new list<Projekt__c>();
        //this.rootNode = new ProjectNode(this.prc);
        this.rootNode = new ProjectNode();
        this.prcs_list = new list<list<Projekt__c>>(); 
        
        this.rootNode = buildAccountRootForNode(rootNodeAcc);
        //System.debug('Build root completed');
        getProjectRoots();
        if(this.rootsProjects != null) {
            //System.debug('Begin structure building');
            this.listMapAN = getDataForLevels();
            //System.debug('Building structure completed');
        }

        this.JSONString = JSON.serialize(this.rootNode);
        
        this.JSONString_param = JSON.serialize(this.rootNode);

        this.chunk_limit = 20;

        //System.debug('BJDEBUG this.prc'+this.prc);
        //System.debug('BJDEBUG rootNode object'+rootNode);
        //System.debug('BJDEBUG_JSONString'+ this.JSONString);
        //System.debug('BJDEBUG_JSONString_param after: '+this.JSONString_param);
        //System.debug('BJDEBUG getVirtualNode'+getVirtualNode());
    } 

    public String currentJSONString() {
        return this.JSONString;
    }

    public void getProjectRoots() {
        //System.debug('$$ rootNodeAcc '+this.rootNodeAcc);
        List<Projekt__c> rootsList;
        if(filterCheckbox == false || filterCheckbox == null) {
            rootsList = new List<Projekt__c>([SELECT  Id, Name, Project_Parent__c, OwnerId, Status__c, RecordTypeId
                        FROM    Projekt__c
                        WHERE   Account__c = :rootNodeAcc.Id
                        AND     Project_Parent__c = null
                        ]);
        } else {
            rootsList = new List<Projekt__c>([SELECT  Id, Name, Project_Parent__c, OwnerId, Status__c, RecordTypeId
                        FROM    Projekt__c
                        WHERE   Account__c = :rootNodeAcc.Id
                        AND     Project_Parent__c = null
                        AND     Status__c != 'Abgeschlossen'
                        ]);
        }
        
        if(rootsList.size() > 0) {
            for(Projekt__c p: rootsList) {
            p.Project_Parent__c = 'a0I1100000FAKEIDEN';
            }
            //System.debug('BJDEBUG rootList = '+rootsList);
            this.rootsProjects = rootsList;
        }
        else {
            /*
            Projekt__c emptyNode = new Projekt__c();
            emptyNode.Name = 'There Is no project for this account';
            emptyNode.Project_Parent__c = 'a0I1100000FAKEIDEN';
            //virtualAcc.OwnerId = rootAccount.OwnerId;
            emptyNode.Status__c = null;
            emptyNode.Project_Parent__c = 'a0I1100000FAKEIDEQ';
            emptyNode.Id = 'a0I1100000FAKEIDEW';
            rootsList.add(emptyNode);*/
            //System.debug('$$$BJ rootsList when no projects = '+ rootsList);
            this.rootsProjects = null;
            //System.debug('$$$BJ rotsProjects after assignment = '+ this.rootsProjects);
        }
    }

    public PageReference filterOutClosedProjects() {
        //System.debug('filterCheckbox = '+filterCheckbox);
        if(filterCheckbox == false || filterCheckbox == null) {
                //this.filterCheckbox = true;
                PrcHierarchyController();
                //System.debug('@@@'+ this.JSONString);
                //System.debug('@@@'+ this.JSONString_param);
            } else {
                //this.filterCheckbox = false;
                PrcHierarchyController();
                //System.debug('@@@'+ this.JSONString);
                //System.debug('@@@'+ this.JSONString_param);
            }
        //System.debug('this.filterCheckbox after change'+this.filterCheckbox);
        return null;
    }
    
    // method for set base url
    public void setSfBaseUrl() {
        sfBaseUrl = URL.getSalesforceBaseUrl().toExternalForm();     
    }
    
    // action for pull new data for tree (4 AJAX)
    
    public PageReference getChunk2() {
        try {
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Chunk Started'));
            
            workProgress = 'Started';
            //System.debug('JSONString_param before: ' + this.JSONString_param);
            //System.debug('prcParentId_param: ' + prcParentId_param);
            //System.debug('this.prcs_param: ' + this.prcs_param);
            //System.debug('this.prcs: ' + this.prcs);
            //System.debug('is listMapAN : ' + listMapAN);
            ProjectNode an_temp;
            ProjectNode an_temp2;
            list<ProjectNode> listTemp = new list<ProjectNode>();
            an_temp = new ProjectNode();
            for (map<ID, ProjectNode> mapAN : listMapAN) {
                //System.debug('mapAN: ' + mapAN);
                //System.debug('an_temp: ' + an_temp);
                an_temp = mapAN.get(prcParentId_param);
                //System.debug('mapAN: ' + mapAN);
                //System.debug('an_temp: ' + an_temp);
                if (an_temp != null) break;
            }
            //System.debug('an_temp: ' + an_temp);
            Integer level = an_temp.level + 1;
            //list<ID> parentsIds = listMapAN[level].keySet();\
            Set<ID> retrievedIds;
            map<ID, ProjectNode> mapTemp;
            if (listMapAN.size() > level) {
                mapTemp = listMapAN[level];
                retrievedIds = mapTemp.keySet();
            }
            else
                mapTemp = new map<ID, ProjectNode>();
            
            this.prcs_param = [ Select  Id, Name, Project_Parent__c, OwnerId, Status__c
                                From    Projekt__c
                                Where   Project_Parent__c = :an_temp.prc.Id//this.prcParentId_param
                                    and Id not in :retrievedIds
                                order by    Name, CreatedDate
                                limit   :this.chunk_limit];
            list<AggregateResult> children = [ Select  Project_Parent__c
                                        From    Projekt__c
                                        Where   Project_Parent__c = :getIds(this.prcs_param)
                                            //and Id not in :retrievedIds
                                        group by    Project_Parent__c];
                                        //limit   :this.chunk_limit];
            Set<ID> idsWithNoChildren = new Set<ID>();
            //idsWithNoChildren = new Set<ID>(clsSobjectList.getIds(children));
            idsWithNoChildren.addAll(getIds(children));
            
            for (Projekt__c prc : prcs_param) {
                an_temp2 = new ProjectNode(prc);
                an_temp2.level = level;
                if (!idsWithNoChildren.contains(an_temp2.prc.Id))
                    an_temp2.noChildren = true;
                //System.debug('### an_temp2: ' + an_temp2);
                //System.debug('### listTemp: ' + listTemp);
                listTemp.add(an_temp2);
                mapTemp.put(an_temp2.prc.Id, an_temp2);
                listMapAN.get(level-1).get(an_temp2.prc.Id);
            }

            mapTemp = new map<ID, ProjectNode>();
            mapTemp.put(an_temp.prc.ID, an_temp);   

            //this.prcs.addAll(this.prcs_param);
            //this.JSONString_param = JSON.serialize(this.prcs_param);
            this.JSONString_param = JSON.serialize(listTemp);
            //System.debug('BJDEBUG_JSONString_param after: ' + this.JSONString_param);
        }
        catch (System.QueryException e) {
            this.JSONString_param = null;
        }
        return null;
    } 
    
    private List<ID> getIds(List<SObject> SObjects) {
        List<ID> Ids = new List<ID>();
        for (SObject so : SObjects) {
            Ids.add(so.Id);
        }
        return Ids;
    }

    public ProjectNode buildAccountRootForNode(Account rootAccount) {
            //System.debug('bulid root rootAccount'+rootAccount);
            Projekt__c virtualAcc = new Projekt__c();
            virtualAcc.Name = rootAccount.Name;
            //virtualAcc.OwnerId = rootAccount.OwnerId;
            virtualAcc.Status__c = null;
            virtualAcc.Project_Parent__c = null;
            virtualAcc.Id = 'a0I1100000FAKEIDEN';
            ProjectNode virtualAccNode = new ProjectNode(virtualAcc);

            virtualAccNode .level = 0;
            return virtualAccNode;
    }

    private list<map<ID, ProjectNode>> getDataForLevels() {
        list<map<ID, ProjectNode>> listMapAN = new list<map<ID, ProjectNode>>();
        ProjectNode an_temp = this.rootNode;
        Integer counter = 0;
        //System.debug('@this.rootNode: '+ this.rootNode);
        //System.debug('#BJDEBUG an_temp 1 = '+an_temp);
        map<ID, ProjectNode> mapTemp = new map<ID, ProjectNode>();
        mapTemp.put(an_temp.prc.ID, an_temp);
        listMapAN.add(mapTemp);
        list<Projekt__c> prcs;
        Set<ID> parentsIds = new Set<Id>{};
        ProjectNode an_temp2;
        Integer level = 0;
        Integer max_sibling2;
        // for limited levels
        //an_temp = new AccountNode();
        ID temp_ID;
        Set<Id> tempIdSet = new Set<Id>{};

        for (level = 0; level < this.max_levels; level++) {
            //System.debug('### level: ' + level);
            /*//System.debug('### an_temp.prc.ID: ' + an_temp.prc.ID);
            //System.debug('### an_temp.prc.Name: ' + an_temp.prc.Name);
            //System.debug('### an_temp: ' + an_temp);*/

            mapTemp = new map<ID, ProjectNode>();
            //System.debug('### BJDEBUG an_temp 2 = '+an_temp);
            if (an_temp != null) {
                if (an_temp.prcs.size() > 0) { // if already has children
                    //System.debug('### if (an_temp.prcs.size() > 0)');
                    an_temp = an_temp.prcs.get(0);

                    //System.debug('#BJDEBUG an_temp 3 = '+an_temp);
                    an_temp.level = level+1;
                    mapTemp.put(an_temp.prc.ID, an_temp);
                    max_sibling2 = max_sibling - 1;
                } else {
                    an_temp = null;
                    max_sibling2 = max_sibling;
                }
                an_temp2 = an_temp;
            }
                
            //System.debug('### an_temp: ' + an_temp);
            //listMapAN[level].remove('a0I11000003RdWHEA0');
            //System.debug('$$$ listMapAN[level] '+listMapAN[level].get('a0I11000003RdW2EAK').prcs);
            //listMapAN[level].get('a0I11000003RdW2EAK').prcs;
            if (level < listMapAN.size()){
                /*
                Map<Id, ProjectNode> listMap = listMapAN[level];
                //System.debug('$$$ listLevel: ' + level);
                //System.debug('$$$ listMapAN '+listMapAN);
                //System.debug('$$$ listMapAN[level] '+listMapAN[level]);
                */

                for(ProjectNode p: listMapAN[level].values()) {
                    if (filterCheckbox == false || filterCheckbox == null) {
                       parentsIds.add(p.prc.Id);
                    } else {
                        if(p.prc.Status__c != 'Abgeschlossen') {
                        parentsIds.add(p.prc.Id);
                        } 
                    }
                    
                }
            }
            
            

            //System.debug('### parentsIds: ' + parentsIds);
            //System.debug('### an_temp.prc.ID: ' + an_temp.prc.ID);
            //System.debug('### an_temp.prc.Name: ' + an_temp.prc.Name);
            //System.debug('### max_sibling+1: ' + max_sibling+1);
            if (an_temp != null) {
                temp_ID = an_temp.prc.ID;
            } else {
                temp_ID = null;
            }
                
            //System.debug('### temp_ID: ' + temp_ID);
            if(level != 0) {
///and Id <> :temp_ID
                if(filterCheckbox == false || filterCheckbox == null) {
                    prcs = [ Select  Id, Name, Project_Parent__c, OwnerId, Status__c, RecordTypeId
                            From    Projekt__c
                            Where   Project_Parent__c in :parentsIds
                            Limit   :max_sibling2+1 ];
                    //System.debug('** prcs after select level: '+ level + ' ' +prcs);
                } else {
                     prcs = [ Select  Id, Name, Project_Parent__c, OwnerId, Status__c, RecordTypeId
                            From    Projekt__c
                            Where   Project_Parent__c in :parentsIds  AND Status__c != 'Abgeschlossen'
                            Limit   :max_sibling2+1 ];
                } 
            }
            else {
                prcs = this.rootsProjects;
            }
            parentsIds.clear();
            //System.debug('### prcs: ' + prcs);
            
            if (prcs.size() == max_sibling2+1) {
                prcs.remove(max_sibling2);
            } else {
                //todo: setNoChildren(listMapAN[level]); // set noChildren to True for parents nodes
                for (ProjectNode an : listMapAN[level].Values()) {
                    an.noChildren = true;
                }
            }
            //System.debug('### BJDEBUG listMapAN'+listMapAN);
            for (Projekt__c prc1 : prcs) {
                an_temp2 = new ProjectNode(prc1);
                an_temp2.level = level+1;
                //System.debug('### BJDEBUG prc1.Project_Parent__c '+prc1.Project_Parent__c);
                //System.debug('### BJDEBUG  listMapAN.get(level)'+listMapAN.get(level));
                listMapAN.get(level).get(prc1.Project_Parent__c).prcs.add(an_temp2); // add child to parent node level
                mapTemp.put(prc1.Id, an_temp2); // add node to current node level
            }
            listMapAN.add(mapTemp);
        }

        //System.debug('### an_temp: ' + an_temp);
        //System.debug('### an_temp2: ' + an_temp2);
        an_temp = an_temp2;
        if (an_temp != null)
            while (an_temp.prcs.size() > 0) {
                an_temp2 = an_temp.prcs.get(0);
                an_temp2.level = ++level;
                //listMapAN.get(level-1).get(an_temp.prc.ID).prcs.add(an_temp2); // add child to parent node level
                mapTemp.put(an_temp2.prc.Id, an_temp2); // add node to current node level
                listMapAN.add(mapTemp);
                an_temp = an_temp2;
            }
        //System.debug('%listMapAN = '+listMapAN[3]);
         //System.debug('%listMapAN = '+ listMapAN[3].get('a0I11000003RdWIEA0')); 
         
        //list<map<ID, ProjectNode>>

        return listMapAN;
    }

 // ------------------------------------------------------- WRAPPER CLASSES DEFINITIONS -------------------------------------------------------
   /************** project Wrapper *************/
    public class ProjectNode {
        public Projekt__c prc;
        public Id ProjectId;
        public list<ProjectNode> prcs;
        public Boolean noChildren;
        public Integer level;
        public Account acc;

        //default constructor
        public ProjectNode() {
            this.prcs = new list<ProjectNode>();
        }
        //Constructor for Projects
        public ProjectNode(Projekt__c prc) {
            this.prc = prepareNode(prc);
            this.ProjectId = prc.Id;
            this.prcs = new list<ProjectNode>();
        }

        public Projekt__c prepareNode(Projekt__c rootNode) {
            //return rootNode;
            string tempValue = '';
            
            list<String> specialCharacters = new List <String>{'\"', '\'', '\\}', '\\{'}; 
            integer index = 0;
            string notSpecial='';
            String ownerName = getUserFullName(rootNode);
            list<String> prcQueriedFields = new list<String>{'Name', 'OwnerId', 'Status__c'};
            for (String entryKey : prcQueriedFields) {
                //System.debug('xxx_checkme ' + rootNode.get(entryKey));
                if (!(entryKey.contains('OwnerId'))) {
                    if(rootNode.get(entryKey) != null){
                        //System.debug('entryKey ' + entryKey + ' doesnt contains any dots');
                        tempValue = (string)rootNode.get(entryKey);
                        //System.debug('xxx_Before - tempValue = ' + tempValue);
                        
                        for (String special : specialCHaracters){
                                notSpecial = '\\' + special;
                            tempValue = tempValue.replace(special, notSpecial);
                            //System.debug('xxx_tempValue = ' + tempValue);
                            notSpecial='';
                        }
                        
                        //System.debug('xxx_After - tempValue = ' + tempValue);
                        
                        rootNode.put(entryKey, tempValue);
                        //System.debug('rootnode = '+rootNode);
                        tempValue='';
                    } 
                } else {
                    
                    if(rootNode.get(entryKey) != null){
                        //System.debug('entryKey ' + entryKey + ' contains any dots');
                        tempValue = ownerName;
                        //System.debug('xxx_Before - tempValue = ' + tempValue);
                        
                        for (String special : specialCHaracters){
                                notSpecial = '\\' + special;
                            tempValue = tempValue.replace(special, notSpecial);
                            //System.debug('xxx_tempValue = ' + tempValue);
                            notSpecial='';
                        }
                        
                        //System.debug('xxx_After - tempValue = ' + tempValue);
                        
                        rootNode.put(entryKey, tempValue);
                        //System.debug('rootnode = '+rootNode);
                        tempValue='';
                    }    
                }    
            }
            //System.debug('xxx_rootNode.prc = ' + rootNode);
            return rootNode;
        }
        
        //pull User names from users
        public String getUserFullName (Projekt__c prc) {
            String pr;
            if(prc.Id == 'a0I1100000FAKEIDEN') {
                pr = 'trolololo';
            }
            else {
            pr = [ Select  Id, OwnerId, Owner.Name
                            From    Projekt__c
                            Where   Id = :prc.Id].Owner.Name;
            //System.debug('pr = '+pr); 
            }
            
            return pr;
        }

        // pull data from parent from DB
        public ProjectNode pullParent(ProjectNode prcn) {
            Projekt__c prc = [ Select  Id, Name, Project_Parent__c, RecordTypeId
                            From    Projekt__c
                            Where   Id = :prcn.prc.Project_Parent__c];
            ProjectNode prcn_parent = new ProjectNode(prc); 
            return prcn_parent;
        }

        // pull/get data for parent Node from parent Projekt__c
        public ProjectNode pullParent() {
            Projekt__c prc;
            try {
                prc = [ Select  Id, Name, Project_Parent__c, OwnerId, Status__c, RecordTypeId
                        From    Projekt__c
                        Where   Id = :this.prc.Project_Parent__c ];
            }
            catch (System.QueryException e) {
                return null;
            }
            return new ProjectNode(prc);
        }

        // build parent for node
        public ProjectNode buildParent() {
            ProjectNode prcn_parent = this.pullParent();
            
            if (prcn_parent != null) {
                prcn_parent.prcs.add(this);
                //this = accn_parent.clone();
            }
            return prcn_parent;
        }
       
        // get Projects from current node and from children (recursive)
        public list<Projekt__c> getProjectList() {
            list<Projekt__c> prcs = new list<Projekt__c>();
            for (ProjectNode pcn : this.prcs) {
                prcs.addAll(pcn.getProjectList());
            }
            prcs.add(prc);
            return prcs;
        }

    }
/************** customized project Wrapper end *************/
}