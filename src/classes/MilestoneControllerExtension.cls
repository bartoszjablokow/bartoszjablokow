public class MilestoneControllerExtension {

    private final Projekt__c project;
    public List<Termine_Milestones__c> termineMilestonesSorted;
    public String projectid;

    
    // The extension constructor initializes the private member
    // variable acct by using the getRecord method from the standard
    // controller.
    public MilestoneControllerExtension(ApexPages.StandardController stdController) {
        this.project = (Projekt__c)stdController.getRecord();
        this.projectId = this.project.Id;
       
    }
    
    public List<Termine_Milestones__c> getTermineMilestonesSorted(){

        this.termineMilestonesSorted = Database.query('SELECT Name, Id, Datum__c, Erledigt__c, Verantwortlich__c, Termin__c, Kommentar__c, To_Do_f_r_Termin__c, Nicht_im_Report__c FROM Termine_Milestones__c where Projekt__c = \'' + projectId + '\' order by Datum__c desc');

        return this.termineMilestonesSorted;
    }
    
    


    public PageReference save() {
        try{ 
           //update this.Termine_Milestones__r;
           System.Debug('projekt ist ' + this.project.name);
           update this.termineMilestonesSorted;
           
        }
        catch(DmlException ex){
            ApexPages.addMessages(ex);  
        }
        PageReference pr = new PageReference('/apex/MilestoneBearbeitung?id=' +  ApexPages.currentPage().getParameters().get('id'));
        pr.setRedirect(true);
        return pr;
    }
    
     
    
    
}